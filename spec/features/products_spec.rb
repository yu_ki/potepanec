require "rails_helper"

feature "Products" do
  describe "商品詳細ページに関するテスト" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    context "商品詳細ページに表示されている内容" do
      scenario "ページタイトルに商品名が含まれているか" do
        expect(page).to have_title product.name
      end

      scenario "lightSection内にHOMEへのリンクと商品名が表示されているか" do
        within ".pageHeader" do
          expect(page).to have_selector "h2", text: product.name
          expect(page).to have_selector "li", text: product.name
          expect(page).to have_link "HOME"
          click_on "HOME"
          expect(current_path).to eq potepan_index_path
        end
      end

      scenario "商品詳細ページにテキストが表示されているか" do
        within ".media-body" do
          expect(page).to have_selector "h2", text: product.name
          expect(page).to have_selector "h3", text: product.display_price.to_s
          expect(page).to have_selector "p", text: product.description
          expect(page).to have_link "一覧ページへ戻る"
        end
      end

      scenario "「一覧ページへ戻る」をクリックでカテゴリーページに戻ること" do
        click_on "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end

      scenario '関連商品が正しく表示されていること' do
        within '.productsContent' do
          expect(page).to have_selector 'h5', text: related_product.name
          expect(page).to have_selector 'h3', text: related_product.display_price.to_s
          expect(page).to have_link related_product.name
          expect(page).to have_link related_product.display_price.to_s
          click_on related_product.name
          expect(current_path).to eq potepan_product_path(related_product.id)
        end
      end

      scenario '関連商品が定数以上ではないこと' do
        within '.productsContent' do
          expect(page).to have_selector '.productBox', count: 4
        end
      end
    end
  end
end
