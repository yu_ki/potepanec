require "rails_helper"

feature "Categories" do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:bag) { create(:taxon, name: "bag", taxonomy: taxonomy) }
  let!(:mug)   { create(:taxon, name: "mug", taxonomy: taxonomy) }
  let!(:productbag) { create(:product, name: "bag", price: 17.99, taxons: [bag]) }
  let!(:productmug) { create(:product, name: "mug", price: 19.99, taxons: [mug]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "サイドバーが機能していること" do
    within ".sideBar" do
      click_on taxonomy.name
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_selector "li", text: "#{taxon.name} (1)"
    end
  end

  scenario "商品詳細ページへ飛び、「一覧ページへ戻る」クリックでカテゴリーページに戻ること" do
    click_on "#{product.name}-img"
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)

    click_on product.name
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)

    click_on product.display_price
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario "カテゴリーページを正しく表示すること" do
    visit potepan_category_path(bag.id)
    within ".productBox" do
      expect(page).to have_content productbag.name
      expect(page).to have_content productbag.display_price
    end
    within ".pageHeader" do
      expect(page).to have_selector "h2", text: bag.name
      expect(page).to have_selector "li", text: bag.name
      expect(page).to have_link "HOME"
      expect(page).to have_link "shop"
      click_on "HOME"
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario "カテゴリーに属さないproduct含まれていないこと" do
    visit potepan_category_path(bag.id)
    within ".productBox" do
      expect(page).not_to have_content productmug.name
      expect(page).not_to have_content productmug.display_price
    end
    within ".pageHeader" do
      expect(page).not_to have_selector "h2", text: mug.name
      expect(page).not_to have_selector "li", text: mug.name
    end
  end
end
