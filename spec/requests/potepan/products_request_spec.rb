require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "レスポンスが成功すること" do
      expect(response).to have_http_status 200
    end

    it "商品名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "価格が表示されていること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "説明文が表示されていること" do
      expect(response.body).to include product.description
    end

    it "関連商品が表示されていること" do
      within '.productsContent' do
        related_products.each do |related_product|
          expect(response.body).to include related_product.name
          expect(response.body).to include related_product.display_price.to_s
        end
      end
    end
  end
end
