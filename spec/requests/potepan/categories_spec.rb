require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "カテゴリーページにて" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "200レスポンスが返ってくること" do
      expect(response).to have_http_status(200)
    end

    it "taxon名が表示されていること" do
      expect(response.body).to include taxon.name
    end

    it "product名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "価格が表示されていること" do
      expect(response.body).to include product.price.to_s
    end
  end
end
