require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "ページタイトル" do
    context "ページタイトルがある場合" do
      it "ページタイトルが表示されていること" do
        expect(full_title("test")).to eq "test - BIGBAG Store"
      end
    end

    context "ページタイトルがない場合" do
      it "BIGBAG Storeのみ表示すること" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "ページタイトルがnilの場合" do
      it "BIGBAG Storeのみ表示すること" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
